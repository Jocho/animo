var Animo = (function() {
	var _a = {
			info: {
				version: '1.2.2',
				author: 'Marek J. Kolcun'
			}
		},
		// basic easing functions
		easeIn = function(power) {return function(t) {return Math.pow(t, power)}},
		easeOut = function(power) {return function(t) {return 1 - Math.abs(Math.pow(t - 1, power));}},
		easeInOut =  function(power) {return function(t) {return t < .5 ? easeIn(power)(t * 2) / 2 : easeOut(power)(t * 2 - 1) / 2 + 0.5;}},
		
		easings = {
			linear: easeInOut(1),
			easeInQuad: easeIn(2),
			easeOutQuad: easeOut(2),
			easeInOutQuad: easeInOut(2),

			easeInCubic: easeIn(3),
			easeOutCubic: easeOut(3),
			easeInOutCubic: easeInOut(3),

			easeInQuart: easeIn(4),
			easeOutQuart: easeOut(4),
			easeInOutQuart: easeInOut(4),

			easeInQuint: easeIn(5),
			easeOutQuint: easeOut(5),
			easeInOutQuint: easeInOut(5)
		},
		time = {
			start: 0,
			end: 0,
			current: 0
		},
		repeat = {
			round: 0,
			totalRounds: 1,
		},
		callbackFn,
		stack = [],
		toFixedPrecision = 2,
		DEFAULT_EASING = 'easeInOutQuad',
		rafId = 0;

	/**
	 * Animo uses .toFixed method to format all calculated values to given decimal points.
	 * Default value of precision is 2 decimal points. You can change this value using this method.
	 * @var precision (int) - integer value of number of decimal points.
	 */
	_a.setPrecision = function(precision) {
		toFixedPrecision = (typeof precision == 'number' && precision >= 0 && precision <= 5) ? Math.floor(precision) : 2;
	};

	/**
	 * Return toFixedPrecision variable value.
	 * @return int - integer value of number of decimal points.
	 */
	_a.getPrecision = function(precision) {
		return toFixedPrecision;
	};

	/**
	 * Starts the animation procedure.
	 */

	_a.start = function(rounds, callbackList, callback) {
		time.start = setTime();
		time.current = time.start;

		repeat.totalRounds = rounds || repeat.totalRounds;
		rafId = _requestAnimationFrame(loop);
		callbackFn = callback || callbackFn;
		callbackList = callbackList || [];
		
		var durationSoFar = 0;

		for (var i = 0, ii = callbackList.length; i < ii; i++) {
			callbackList[i]();

			durationSoFar = _a.getTotalDuration();

			for (var j = 0, jj = stack.length; j < jj; j++) {
				stackDuration = stack[j].getTotalDuration();
				if (stackDuration < durationSoFar)
					stack[j].wait(durationSoFar - stackDuration);
			}
		}
		setStopTime();
	};

	_a.pause = function () {
		_cancelAnimationFrame(rafId);
	}

	_a.continue = function() {
		var timeDiff = time.current - time.start;
		time.start = setTime() - timeDiff;
		setStopTime();

		rafId = _requestAnimationFrame(loop);
	}

	_a.stop = function(fullStop) {
		fullStop = typeof fullStop === 'boolean' ? fullStop : false;
		_cancelAnimationFrame(rafId);

		repeat.round++;

		if (!fullStop && (repeat.round < repeat.totalRounds || repeat.totalRounds == -1)) {
			_a.resetAnimation();
		}
		else if (typeof callbackFn === 'function') {
			repeat.round = 0;
			var cb = callbackFn;
			callbackFn = null;
			cb();
		}
	};

	_a.resetAnimation = function() {
		for (var i = 0, ii = stack.length; i < ii; i++) {
			stack[i].reset();
		}
		_a.start();
	};


	/**
	 * Adds created Animo Element into Animo processes.
	 * @see _a.element
	 * @var element (HTML Element)
	 */
	_a.add = function(element) {
		if (element instanceof Array) {
			stack = element;

			for (var i = 0, ii = stack.length; i < ii; i++)
				element[i].id = i;
			// return Object.keys(stack);
		}
		else {
			stack.push(element);
			element.id = stack.length - 1;
		}
	};

	_a.update = function(element) {
		element = (element instanceof Array) ? element : [element];

		for (var x = 0, xx = element[x].length; x < xx; x++) {
			var stages = element[x].getStages(),
				i = stack[element[x].id].getStages().length,
				ii = stages.length;

			for (i, ii; i < ii; i++) {
				stack[element[x].id].set(stages[i].varval, stages[i].duration, stages[i].easing);
			}
		}
	};

	_a.getStack = function() {
		return stack;
	};


	_a.remove = function(stackId) {
		stack.splice(stackId, 1);
	};

	/**
	 * Sets the time when the animatino stops.
	 */
	var setStopTime = function() {
		time.end = time.start + _a.getTotalDuration();
	}

	/**
	 * Main animation function. Sets the current time and runs animate() method on all of the stacks.
	 * When the current time is larger than calculated animatio endtime, the animation stops.
	 */
	var loop = function() {
		time.current = setTime();

		for (var i = 0, ii = stack.length; i < ii; i++) {
			stack[i].animate();
		}

		rafId = _requestAnimationFrame(loop);

		if (time.end <= time.current)
			_a.stop();
	};

	/**
	* Handle to the callback-routine
	*/
	var _requestAnimationFrame = (function() {
		return window.requestAnimationFrame
			|| window.webkitRequestAnimationFrame
			|| window.mozRequestAnimationFrame
			|| window.oRequestAnimationFrame
			|| window.msRequestAnimationFrame
		// Fallback
			|| function( callback ) {
				return window.setTimeout( callback, 1000 / 60 );
			};
	})(),

	/**
	* Handle to cancel the routine
	*/
	_cancelAnimationFrame = (function() {
		return window.cancelAnimationFrame
			|| window.cancelRequestAnimationFrame
			|| window.webkitCancelAnimationFrame
			|| window.webkitCancelRequestAnimationFrame
			|| window.mozCancelAnimationFrame
			|| window.mozCancelRequestAnimationFrame
			|| window.msCancelAnimationFrame
			|| window.msCancelRequestAnimationFrame
			|| window.oCancelAnimationFrame
			|| window.oCancelRequestAnimationFrame
			|| window.clearTimeout;
	})();

	var setTime = function(value) {
		return (typeof value === 'number') ? value : new Date().getTime();
	};

	_a.getTime = function(type) {
		if (typeof type === 'string' && typeof time[type] !== 'undefined')
			return time[type];
		return time;
	};

	/**
	 * Gets total animation duration in miliseconds
	 */
	_a.getTotalDuration = function() {
		var elemDuration = 0, totalDuration = 0;
		for (var i = 0, ii = stack.length; i < ii; i++) {
			elemDuration = stack[i].getTotalDuration();

			if (elemDuration > totalDuration)
				totalDuration = elemDuration;
		}
		return totalDuration;
	};

	/**
	 * Returns the easing function defined in each stage.
	 * @var easing (string)
	 * @return function - easing function from `easings` object.
	 */
	_a.getEasing = function(easing) {
		return (typeof easings[easing] !== 'undefined') ? easings[easing] : easings[DEFAULT_EASING];
	};


	/**
	 * Creates an extended HTML Element based on selector
	 * and enhances it with various animation-related variables and methods.
	 * @var selector (string) - standard document.querySelector() selector
	 * @return Object         - returns Animo Element, object suitable be processed later by Animo.
	 */
	_a.element = function(selector) {
		var _e = {},
			element = document.querySelector(selector),
			defaults = {
				left: 0,
				top: 0,
				rotate: 0,
				scale: 1,
				opacity: 1
			},
			currents = {
				left: 0,
				top: 0,
				rotate: 0,
				scale: 1,
				opacity: 1
			},
			stages = [],
			REGEXP = new RegExp('(-?\\\d*(\\\.\\\d*)?)((px)|(r?em)|(\\\%))'),
			PREFIXES = ['-moz-', '-webkit-', '-ms-', ''],
			PREFIXED = ['rotate', 'scale'];

		/**
		 * Sets new values to animatable variables.
		 * values are set with `precision` and easing function in mind in a defined duration.
		 * @var varval (object) - object with variable and new value
		 * @var duration (int)  - duration of transforming old values into new, in miliseconds
		 * @easing (string)     - used easing function. If not defined, 'easeInOut' is used.
		 * @return _a.element (object)
		 */
		_e.set = function(varval, duration, easing) {
			stages.push({
				varval: varval,
				duration: duration,
				easing: easing || 'easeInOut'
			});
			return this;
		};

		/**
		 * When used with (true) value, it redefines default element animation values
		 * and assignes them current element state values.
		 * @var keepDefaults (boolean) - when set to true, values are redefined
		 * @return _a.element (object)
		 */
		_e.clear = function(keepDefaults) {
			stages = [];
			keepDefaults = (typeof keepDefaults == 'boolean') ? keepDefaults : false;

			if (!keepDefaults) {
				for (var i in currents)
					defaults[i] = currents[i];
			}
			return this;
		};

		/**
		 * Sets default animation values that an element had defined during initialization
		 * as current animation values.
		 * @return _a.element (object)
		 */
		_e.reset = function() {
			for (var i in defaults) {
				_e.setStyle(i, defaults[i]);
			}
			return this;
		};

		/**
		 * Assigns the last defined animation values in linear easing for a given time.
		 * Running the animation is necessary, but as the values don't change, element
		 * seems to be stopped and does not animate.
		 * @var duration (int) - waiting duration in miliseconds
		 * @return _a.element (object)
		 */
		_e.wait = function(duration) {
			var prevVarval = getStageSettings(stages.length - 1);

			_e.set(prevVarval, duration, 'linear');
			return this;
		};

		/**
		 * Updates the values of animated variables.
		 * Method first evaluates the difference between previous value and a value,
		 * that should be set according to used easing function in given time.
		 * That value is then added to the previous value and updated in styles.
		 */
		_e.animate = function() {
			var data = getStage(),
				easing, newVal,
				finalVal, prevVarval,
				prevVal, diff;

			prevVarval = getStageSettings(data.id - 1);
			
			for (var i in data.stage.varval) {
				prevVal = (typeof prevVarval[i] !== 'undefined') ? prevVarval[i] : defaults[i];
				prevVal = parseValue(prevVal);
				finalVal = parseValue(data.stage.varval[i]);

				diff = parseFloat(finalVal[0]) - parseFloat(prevVal[0]);

				easing = _a.getEasing(data.stage.easing);
				newVal = frmt(parseFloat(prevVal[0]) + diff * easing(data.percent)) + finalVal[1];

				setStyleValue(i, newVal);
			}
		};

		/**
		 * private function to format the numeric value on pre-defined decimal spaces.
		 * @var value (float) - value to be formatted
		 * @return float      - returned formatted value with set decimal spaces
		 */
		var frmt = function(value, decimals) {
			decimals = decimals || _a.getPrecision();
			if (typeof value.toFixed === 'function')
				return parseFloat(value.toFixed(decimals));
			return value;
		};

		/**
		 * Checks, whether given value contains any units (em, rem, px, %).
		 * If it does, value is parsed into numeric part and unit part for further evaluation/assignment.
		 * @var value (int|float|string) - value of the animated variable
		 * @return array                 - numeric value on index 0 and unit value on index 1
		 */
		var parseValue = function(value) {
			if (typeof value === 'string') {
				var match = value.match(REGEXP);
				return match ? [parseFloat(match[1]), match[3]] : [value, ''];
			}

			return [value, ''];
		};

		/**
		 * Gets the value of animated variable from Element's CSS.
		 * If the variable is used with prefixes, it gets last defined prefixed value.
		 * @var variable (string) - variable name
		 * @return mixed (int|float|string) - value of the variable obtained from CSS
		 */
		var getStyleValue = function(variable) {
			var prefix,
				cs = window.getComputedStyle(element);


			if (PREFIXED.indexOf(variable) > -1) {
				for (var i = 0, ii = PREFIXES.length; i < ii; i++) {
					prefix = PREFIXES[i] + 'transform';
					if (typeof cs.getPropertyValue(prefix) !== 'undefined')
						return getTransformValue(cs.getPropertyValue(prefix), variable);
				}
			}
			else {
				return cs.getPropertyValue(variable);
			}
		};

		/**
		 * 
		 */
		_e.setStyle = function(variable, value, rewriteDefault) {
			if (typeof variable === 'object') {
				rewriteDefault = value;

				for (var i in variable) {
					setStyleValue(i, variable[i], rewriteDefault);
				}
			}
			else
				setStyleValue(variable, value, rewriteDefault);
		};

		/**
		 * Saves given variable value into Element's CSS declaration.
		 * @var variable (string)     - variable name
		 * @var value (mixed)         - new variable value
		 * @var rewriteDefault (bool) - if true, rewrite element's default variable value, otherwise don't
		 */
		var setStyleValue = function(variable, value, rewriteDefault) {
			if (PREFIXED.indexOf(variable) > -1) {
				var stv = setTransformValue(variable, value);
				
				for (var i = 0, ii = PREFIXES.length; i < ii; i++) {
					prefix = PREFIXES[i] + 'transform';

					if (1 || typeof element.style[prefix] !== 'undefined') {
						element.style[prefix] = stv;
					}
				}
			}
			else {
				element.style[variable] = value;
			}
			
			if (rewriteDefault)
				defaults[variable] = value;

			currents[variable] = value;
		};

		/**
		 * Traverses element's animation stage defined by its id
		 * and return variable values the Element will have set after the stage is done.
		 * If stage doesn't change any animated variable, default value will be used.
		 * @var id (int)  - index of the element's animation stage
		 * @return object - key-value pairs of the stages' settings
		 */
		var getStageSettings = function(id) {
			var settings = {};
			
			if (id < 0)
				return defaults;

			for (var i = id; i >= 0; i--) {
				for (var j in stages[i].varval) {
					if (typeof settings[j] === 'undefined') {
						settings[j] = stages[i].varval[j];
					}
				}

				if (setupedAll(settings))
					break;
			}

			for (var i in defaults) {
				if (typeof settings[i] === 'undefined') {
					settings[i] = defaults[i];
				}
			}
			return settings;
		};

		/**
		 * Checks, whether stage sets every animation variable.
		 * @var settings - `varval` object defined in stage
		 * @return bool  - true if each element's animation variable was set, false otherwise.
		 */
		var setupedAll = function(settings) {
			for (var i in defaults) {
				if (typeof settings[i] === 'undefined') {
					return false;
				}
			}
			return true;
		};

		/**
		 * Parses CSS `transform` variable and gets its scale and rotate values.
		 * @var transform (string) - CSS transform value
		 * @var transVar (string)  - value we search for in transform variable's value declaration
		 * @return mixed (int|float|string) - value of wanted transform variable
		 */
		var getTransformValue = function(transform, transVar) {
			var vals = transform.match(/.+?\((.+)\)/);
			if (vals === null) {
				switch (transVar) {
					case 'scale': return 1;
					case 'rotate': return 0;
				}
			}

			vals = vals[1].split(',');
			var a = vals[0];
			var b = vals[1];

			switch (transVar) {
				case 'scale': return Math.sqrt(a * a + b * b);
				case 'rotate': return Math.round(Math.atan2(b, a) * (180 / Math.PI));
			}
		};

		/**
		 * Sets Element's CSS transform variable to a new value.
		 * @var transVar (string) - name of the transform variable (rotate|scale)
		 * @var transVal (mixed)  - new value of transform variable
		 * @return string         - new CSS transform variable value, ready to set
		 */
		var setTransformValue = function(transVar, transVal) {
			var ret = [], v;
			for (var i = 0, ii = PREFIXED.length; i < ii; i++) {
				v = (transVar == PREFIXED[i]) ? frmt(transVal) : currents[PREFIXED[i]];
				if (PREFIXED[i] == 'rotate')
					v += 'deg';
				ret.push(PREFIXED[i] + '(' + v + ')');
			}
			
			return ret.join(' ');
		};

		/**
		 * Finds the current stage that, according to the current animation time,
		 * should be processed, as well as it's percentual completion.
		 * @return object - contains index of the stage, it's values and percentual stage it should be in.
		 */
		var getStage = function() {
			var duration = _a.getTime('current') - _a.getTime('start'),
				timeSum = 0;
			
			for (var i = 0, ii = stages.length; i < ii; i++) {
				timeSum += stages[i].duration;

				if (duration <= timeSum) {
					return {
						id: i,
						stage: stages[i],
						percent: (timeSum) ? frmt((duration - timeSum + stages[i].duration) / stages[i].duration) : 0
					}
				}
			}

			return {
				id: -1,
				stage: {},
				percent: 0
			};
		};

		/**
		 * Gets total animation duration of given element.
		 * Sums up duration of each stage the element has defined and returns it.
		 * @return int - total element animation duratino in miliseconds
		 */
		_e.getTotalDuration = function() {
			var totalDuration = 0;
			for (var i = 0, ii = stages.length; i < ii; i++) {
				totalDuration += stages[i].duration;
			}

			return totalDuration;
		};

		/**
		 * Getter of an Animo element
		 * @return object (_a.element)
		 */
		_e.elem = function() {
			return element;
		};
		
		/**
		 * Returns element's default animation variable values.
		 * Caution, default values can be rewritten!
		 * @see _e.setStyle
		 * @see _e.clear
		 * @return object - default animation variable values.
		 */
		_e.getDefaults = function() {
			return defaults;
		};

		/**
		 * Returns element's current animation variable values.
		 * @return object - current animation variable values.
		 */
		_e.getCurrents = function() {
			return currents;
		};

		/**
		 * Returns list of all defined element animation stages.
		 * @return array - list of animation stages
		 */
		_e.getStages = function() {
			return stages;
		};

		/**
		 * Defines Element's `defaults` values, as well as `current` values.
		 */
		var init = function() {
			var sv;
			for (var i in defaults) {
				sv = getStyleValue(i);
				defaults[i] = (sv) ? sv : defaults[i];
				currents[i] = defaults[i];
			}
		};

		init();
		return _e;
	};

	return _a;
});