/**
 * @author Marek J. Kolcun
 * @version 0.2.1
 */
var Animo = (function() {
	var _a = {};

	_a.tl = [];
	_a.currentTimeline = 0;
	_a.targetTimeline = 0;
	_a.repeat = {
		times: 0,
		callback: null
	};

	var timeline = [];

	// basic easin functions
	var	easeIn = function(power) {return function(t) {return Math.pow(t, power)}};
	var easeOut = function(power) {return function(t) {return 1 - Math.abs(Math.pow(t - 1, power));}};
	var easeInOut =  function(power) {return function(t) {return t < .5 ? easeIn(power)(t * 2) / 2 : easeOut(power)(t * 2 - 1) / 2 + 0.5;}};
	var toFixedPrecision = 2;

	var easings = {
		linear: easeInOut(1),
		easeInQuad: easeIn(2),
		easeOutQuad: easeOut(2),
		easeInOutQuad: easeInOut(2),

		easeInCubic: easeIn(3),
		easeOutCubic: easeOut(3),
		easeInOutCubic: easeInOut(3),

		easeInQuart: easeIn(4),
		easeOutQuart: easeOut(4),
		easeInOutQuart: easeInOut(4),

		easeInQuint: easeIn(5),
		easeOutQuint: easeOut(5),
		easeInOutQuint: easeInOut(5)
	};

	_a.info = {
		author: 'Marek J. Kolcun',
		version: '0.2.1',
		web: 'http://jocho.sk'
	}

	/**
	 * Animo uses .toFixed method to format all calculated values to given decimal points.
	 * Default value of precision is 2 decimal points. You can change this value using this method.
	 * @var precision (int) - integer value of number of decimal points.
	 */
	_a.setPrecision = function(precision) {
		toFixedPrecision = (typeof precision == 'number' && precision >= 0 && precision <= 5) ? Math.floor(precision) : 2;
	};

	/** 
	 * Animo element obtained by selector will send its CSS styles to internal data object.
	 * Then Animo will be able to animate this HTML element.
	 * @var selector HTML element - element/list of elements to be animated
	 * @var defaults object 	  - element can be given default CSS values via this object.
	 * 								Currently supported: top, left, width, height, opacity, transform
	 */
	_a.elem = function(selector, defaults) {
		_e = {};

		var elems = document.querySelectorAll(selector);
		var timesheet = [];
		var processAnimationList = false;

		var regs = {
			pos: new RegExp(/(-?\d+(\.\d+)?) ?(\D+)?/),
			rot: new RegExp(/(\d+(\.\d+)?) ?((deg)?)/),
			scl: new RegExp(/(\d+(\.\d+)?)/)
		};

		const STEP = 1000 / 60;
		const PREFIXES = ['-webkit-', '-moz-', '-ms-', ''];
		const DEFAULT_EASING = 'easeInOutQuad';
		
		defaults = defaults || [];
		var initDefaults = (defaults.length) ? false : true;

		/**
		 * @return elem instance (because of recursion)
		 */
		_e.me = function() {
			return this;
		};

		var me = _e.me();

		/**
		 * Initialisation of Animo element. Checks for list of defined elements, sets their defaults.
		 */
		var init = function() {
			for (var i = 0, ii = elems.length; i < ii; i++) {
				elems[i].style.position = 'absolute';
				var cs = window.getComputedStyle(elems[i]);

				if (initDefaults)
					defaults.push({
						top: cs.top,
						left: cs.left,
						width: cs.width,
						height: cs.height,
						opacity: parseFloat(cs.opacity),
						transform: parseTransform(cs)
					});
			}
		};

		var getPos = function(parsed, position) {
			parsed = new String(parsed).match(regs.pos);
			var ret = [];

			for (var i = 0, ii = defaults.length; i < ii; i++) {
				var def = defaults[i][position].match(regs.pos);
				
				if (def == null)
					def = [0, 'px'];

				def[1] = parseFloat(def[1]);
				parsed[1] = parseFloat(parsed[1]);

				var dVal = !isNaN(def[1]) ? def[1] : 0;
				var dPar = typeof def[3] !== 'undefined' && def[3].length > 0 ? def[3] : 'px';

				var value = !isNaN(parsed[1]) ? parsed[1] : dVal;
				var param = typeof parsed[3] !== 'undefined' && parsed[3].length > 0 ? parsed[3] : dPar;
				
				ret.push([value, dVal, param]);
			}
			return ret;
		};

		var getAngle = function(angle) {
			var ret = [];
			for (var i = 0, ii = defaults.length; i < ii; i++) {
				var def = defaults[i].transform.rotate;
				var dVal = !isNaN(def) ? def : 0;
				var value = (typeof angle === 'number') ? angle : def;

				if (angle == 'random')
					value = (720 * Math.random()) - 360;
				ret.push([value, dVal]);
			}
			return ret;
		};

		var getScale = function(scale) {
			var ret = [];
			for (var i = 0, ii = defaults.length; i < ii; i++) {
				var def = defaults[i].transform.scale;
				var dVal = !isNaN(def) ? def : 0;
				var value = (typeof scale === 'number') ? scale : def;
				if (scale == 'random')
					value = frmt(Math.random());
				ret.push([value, dVal]);
			}
			return ret;
		};

		var getOpacity = function(opacity) {
			var ret = [];
			for (var i = 0, ii = defaults.length; i < ii; i++) {
				var def = defaults[i].opacity;
				var dVal = !isNaN(def) ? def : 0;
				var value = (typeof opacity === 'number') ? opacity : def;
				if (opacity == 'random')
					value = frmt(Math.random());
				ret.push([value, dVal]);
			}
			return ret;
		};

		var getDuration = function(duration) {
			return (typeof duration === 'number' && duration > STEP) ? duration : STEP;
		};

		var getEasing = function(easing) {
			return (typeof easings[easing] !== 'undefined') ? easing : DEFAULT_EASING;
		};

		var parseTransform = function(st) {
			var tr = st.getPropertyValue("-webkit-transform") ||
			st.getPropertyValue("-moz-transform") ||
			st.getPropertyValue("-ms-transform") ||
			st.getPropertyValue("transform") || '';
			
			var vals = tr.match(/.+?\((.+)\)/);
			if (vals === null)
				return {scale: 1, rotate: 0};

			vals = vals[1].split(',');

			var a = vals[0];
			var b = vals[1];
			// var c = vals[2];
			// var d = vals[3];
			var scale = Math.sqrt(a * a + b * b);
			var angle = Math.round(Math.atan2(b, a) * (180 / Math.PI));
			
			return {scale: frmt(scale), rotate: angle};
		};

		var setTransform = function(i, fn, value) {
			var defs = defaults[i];
			var ret = [];

			for (var j in defs.transform) {
				if (!defs.transform.hasOwnProperty(j))
					continue;
				var v = (fn == j) ? frmt(value) : defs.transform[j];

				if (j == 'rotate' && fn != 'rotate')
					v += 'deg';

				ret.push(j + '(' + v + ')');
			}
			return ret.join(' ');
		};

		var callNextAni = function(pal) {
			if (pal && animationList.length) {
				var a = animationList.shift();
				me.set(a[0], a[1], a[2], a[3]);
			}
		};

		var frmt = function(value) {
			if (typeof value.toFixed === 'function')
				return parseFloat(value.toFixed(toFixedPrecision));
			return value;
		};

		init();

		var moveVar = function(variable, value, duration, easing, abs) {
			duration = getDuration(duration);
			easing = getEasing(easing);
			abs = (typeof abs === 'boolean') ? abs : false;
			
			duration = getDuration(duration);
			easing = getEasing(easing);

			var pal = processAnimationList;
			var tstep = duration / (duration / STEP);
			var tsteps = 0;
			var positions = getPos(value, variable);
			var newPos, coef, finalVal;

			var int = setInterval(function() {
				coef = frmt(easings[easing](tsteps / duration));
				
				if (tsteps <= duration) {
					for (var i = 0, ii = elems.length; i < ii; i++) {
						newPos = abs ? positions[i][0] - positions[i][1] : positions[i][0];
						finalVal = frmt(newPos * coef + positions[i][1]) + positions[i][2]
						elems[i].style[variable] = finalVal;
						defaults[i][variable] = finalVal;
					}
					tsteps += tstep;
				}
				else {
					callNextAni(pal);
					clearInterval(int);
				}
			}, STEP);

			return me;
		};

		_e.moveX = function(x, duration, easing, abs) {
			return moveVar('left', x, duration, easing, abs);
		};

		_e.moveY = function(y, duration, easing, abs) {
			return moveVar('top', y, duration, easing, abs);
		};

		_e.width = function(x, duration, easing, abs) {
			return moveVar('width', x, duration, easing, abs);
		};

		_e.height = function(y, duration, easing, abs) {
			return moveVar('height', y, duration, easing, abs);
		};

		_e.dimension = function(xy, duration, easing, abs) {
			duration = getDuration(duration);
			easing = getEasing(easing);

			if (xy.length < 2) 
				return;

			var pal = processAnimationList;
			processAnimationList = false;
			this.width(xy[0], duration, easing, abs);
			processAnimationList = pal;
			this.height(xy[1], duration, easing, abs);
		}

		_e.move = function(xy, duration, easing, abs) {
			duration = getDuration(duration);
			easing = getEasing(easing);

			if (xy.length < 2) 
				return;

			var pal = processAnimationList;
			processAnimationList = false;
			this.moveX(xy[0], duration, easing, abs);
			processAnimationList = pal;
			this.moveY(xy[1], duration, easing, abs);
		};

		_e.rotate = function(angle, duration, easing, abs) {
			abs = (typeof abs === 'boolean') ? abs : false;
			duration = getDuration(duration);
			easing = getEasing(easing);

			var pal = processAnimationList;
			var tstep = duration / (duration / STEP);
			var tsteps = 0;
			var angles = getAngle(angle);
			var newAngle, coef, finalVal;
			
			var int = setInterval(function() {
				if (tsteps <= duration) {
					for (var i = 0, ii = elems.length; i < ii; i++) {
						newAngle = (abs) ? angles[i][0] - angles[i][1] : angles[i][0];
						coef = easings[easing](tsteps / duration);
						finalVal = frmt(newAngle * coef + angles[i][1]);

						for (var j = 0; j < PREFIXES.length; j++)
							elems[i].style[PREFIXES[j] + 'transform'] = setTransform(i, 'rotate', finalVal + 'deg');
						
						defaults[i].transform.rotate = finalVal;
					}
					tsteps += tstep;
				}
				else {
					callNextAni(pal);
					clearInterval(int);
				}
			}, STEP);

			return this;
		};

		_e.scale = function(ratio, duration, easing) {
			ratio = ratio > 0 ? ratio : 0;
			duration = getDuration(duration);
			easing = getEasing(easing);

			var pal = processAnimationList;
			var tstep = duration / (duration / STEP);
			var tsteps = 0;
			var ratios = getScale(ratio);
			var newRatio, coef, finalVal;
			
			var int = setInterval(function() {
				if (tsteps <= duration) {
					for (var i = 0, ii = elems.length; i < ii; i++) {
						newRatio = ratios[i][0] - ratios[i][1];
						coef = easings[easing](tsteps / duration);
						finalVal = newRatio * coef + ratios[i][1];

						for (var j = 0; j < PREFIXES.length; j++)
							elems[i].style[PREFIXES[j] + 'transform'] = setTransform(i, 'scale', finalVal);
						defaults[i].transform.scale = finalVal;
					}
					tsteps += tstep;
				}
				else {
					callNextAni(pal);
					clearInterval(int);
				}
			}, STEP);

			return this;
		};

		_e.opacity = function(opacity, duration, easing) {
			if (opacity > 1)
				opacity = 1;
			if (opacity < 0)
				opacity = 0;
			duration = getDuration(duration);
			easing = getEasing(easing);
			
			var pal = processAnimationList;
			var tstep = duration / (duration / STEP);
			var tsteps = 0;
			var opacities = getOpacity(opacity);
			var newOpacity, coef, finalVal;
			
			var int = setInterval(function() {
				if (tsteps <= duration) {
					for (var i = 0, ii = elems.length; i < ii; i++) {
						newOpacity = opacities[i][0] - opacities[i][1];
						coef = easings[easing](tsteps / duration);
						finalVal = newOpacity * coef + opacities[i][1];
						elems[i].style.opacity = finalVal;
						defaults[i].opacity = finalVal;
					}
					tsteps += tstep;
				}
				else {
					callNextAni(pal);
					clearInterval(int);
				}
			}, STEP);

			return this;
		};

		_e.show = function(duration, easing) {
			return this.opacity(1, duration, easing);
		};

		_e.hide = function(duration, easing) {
			return this.opacity(0, duration, easing);
		};

		_e.set = function(params, duration, easing, abs) {
			var i = 0;
			var ii = Object.keys(params).length;
			
			for (var param in params) {
				i++;
				processAnimationList = i == ii;
				
				if (!params.hasOwnProperty(param) || typeof this[param] !== 'function')
					continue;

				switch (param) {
					case 'wait':
					case 'addClass':
					case 'removeClass':
						this[param](params[param]);
						break;
					
					case 'scale':
					case 'show':
					case 'hide':
					case 'opacity':
						this[param](params[param], duration, easing);
						break;
					
					default:
						this[param](params[param], duration, easing, abs);
						break;
				}

			}
			return this;
		};

		_e.wait = function(duration) {
			duration = getDuration(duration);
			var pal = processAnimationList;

			setTimeout(function() {
				callNextAni(pal);
			}, duration);
		};

		_e.getMe = function(selector) {
			var slctr = selector.replace('.', ' ').trim();
			var isId = selector.indexOf('#') > -1;
			var index = -1;
			for (var i = 0, ii = elems.length; i < ii; i++) {
				if ((isId && getAttribute('id') == slctr) || elems[i].className.match(slctr)) {
					return _a.elem(selector, [defaults[i]]);
				}
			}
		};

		_e.getElem = function(i) {
			if (typeof elems[i] !== 'undefined')
				return elems[i];
			return elems;
		};

		_e.getDefaults = function(i) {
			if (typeof defaults[i] !== 'undefined')
				return defaults[i];
			return defaults;
		};

		_e.getAnimationList = function() {
			return animationList;
		}

		_e.addClass = function(className) {
			var pal = processAnimationList;
			var elems = this.getElem();
			for (var i = 0, ii = elems.length; i < ii; i++) {
				elems[i].classList.add(className);
			}

			setTimeout(function() {
				callNextAni(pal);
			}, STEP);
		};

		_e.removeClass = function(className) {
			var pal = processAnimationList;
			var elems = this.getElem();
			for (var i = 0, ii = elems.length; i < ii; i++) {
				elems[i].classList.remove(className);
			}

			setTimeout(function() {
				callNextAni(pal);
			}, STEP);
		};

		var animationList = [];
		
		_e.ani = function(params, duration, easing, abs) {
			duration = getDuration(duration);
			easing = getEasing(easing);
			abs = (typeof abs === 'boolean') ? abs : false;

			animationList.push([params, duration, easing, abs]);

			return this;
		};

		_e.mo = function() {
			if (animationList.length) {
				var a = animationList.shift();
				this.set(a[0], a[1], a[2], a[3]);
			}
		};

		_e.unani = function(i) {
			if (typeof animationList[i] !== 'undefined') {
				var unani = animationList.splice(i, 1);
			}
			else
				animationList = [];
		};

		return _e;
	};

	_a.timeline = function(list) {
		this.currentTimeline = 0;
		this.tl = (list instanceof Array) ? list : [];
		return this;
	};

	_a.getTimeline = function(i) {
		return (typeof this.tl[i] !== 'undefined') ? this.tl[i] : this.tl;
	};

	_a.cb = function(i, delay)  {
		i.currentTimeline++;
		var tl = i.getTimeline(i.currentTimeline);

		if (typeof tl === 'function' && i.currentTimeline <= i.targetTimeline) {
			setTimeout(function() {
				tl(i);
			}, delay);
		}
		else {
			if (i.repeat.times) {
				i.repeat.times--;
				setTimeout(function() {
					i.reset();
				}, delay);
			}
		}
	};

	_a.run = function(curTl, tarTl) {
		this.currentTimeline = (typeof this.tl[curTl] !== 'undefined') ? (curTl - 1) : -1;
		this.targetTimeline = (typeof tarTl === 'number' && this.currentTimeline < tarTl) ? tarTl : this.tl.length;
		this.cb(this, 0);
	};

	_a.reset = function(timeout) {
		var me = this;
		setTimeout(function() {
			me.currentTimeline = -1;
			me.run();
		}, timeout);
	};

	_a.repeatRun = function(times) {
		this.repeat.times = (typeof times === 'number') ? times : 0;
		this.run();
	};

	return _a;
});